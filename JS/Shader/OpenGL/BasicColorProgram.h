#ifndef _JS_BASIC_COLOR_PROGRAM_H_HJS_20170323_
#define _JS_BASIC_COLOR_PROGRAM_H_HJS_20170323_

#include <cstdint>
#include <string>
#include <map>
#include <array>

#include "ShaderProgram.h"

namespace JS
{
    namespace Shader
    {
        class BasicColorProgram : public Program
        {
        private:
            enum eUNIFORM
            {
                UNI_MVPMatrix = 1
            };
            
        public:
            BasicColorProgram();
            ~BasicColorProgram();
            
            void SetUniformMVPMatrix(const float * matrix);
            
            const static char * shader_code_vs;
            const static char * shader_code_fs;
            
        protected:
            std::map<uint32_t, int32_t> m_mapBindUniform;
            
        protected:
            void _BindAttributes();
            void _BindUniforms();
        };
    }
}

#endif
