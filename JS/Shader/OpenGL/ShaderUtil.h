#ifndef _JS_SHADER_UTIL_H_HJS_20170323_
#define _JS_SHADER_UTIL_H_HJS_20170323_

#include <cstdint>
#include <vector>
#include <string>
#include <map>
#include <array>

namespace JS
{
	namespace Shader
	{
		bool CompileVertexShader(uint32_t & id, const char * shader);
		bool CompileFragmentShader(uint32_t & id, const char * shader);

		void GetUniformID(std::map<uint32_t, int32_t> & out, const std::map<uint32_t, std::string> & in, const uint32_t & pid);
		void BindAttributeID(const std::map<uint32_t, std::string> & in, const uint32_t & pid);

	}
}
#endif // !_JS_SHADER_UTIL_H_HJS_20170323_

