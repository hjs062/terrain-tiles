#include "ShaderProgram.h"
#include "ShaderUtil.h"
#include "../../GraphicsAPI/OpenGL/OpenGLHeader.h"

#include <vector>

namespace JS
{
	namespace Shader
	{
		Program::Program()
			: m_id(0)
		{
		}

		Program::~Program()
		{
			if(0 != m_id )
			{
				glDeleteProgram(m_id);
			}
		}

		bool Program::init(const uint32_t & vs, const uint32_t & fs)
		{
			if( 0 == (m_id = glCreateProgram()))
            {
                return false;
            }

			// Attach the fragment and vertex shaders to it
			glAttachShader(m_id, fs);
			glAttachShader(m_id, vs);

            // Before to link a program, bind attributes.
			_BindAttributes();

			// Before to bind uniforms, link a program.
			glLinkProgram(m_id);

			// Check if linking succeeded in the same way we checked for compilation success
			int32_t bLinked;
			glGetProgramiv(m_id, GL_LINK_STATUS, &bLinked);
			if (!bLinked)
			{
				int infoLogLength, numCharsWritten;
				glGetProgramiv(m_id, GL_INFO_LOG_LENGTH, &infoLogLength);
				std::vector<char> infoLog; infoLog.resize(infoLogLength);
				glGetProgramInfoLog(m_id, infoLogLength, &numCharsWritten, infoLog.data());

				std::string message("Failed to link program: ");
				message += infoLog.data();
				return false;
			}
            
			_BindUniforms();

            // glDeleteShader does not just delete a shader but decrement a reference count for the shader.
            // when a reference count is zero, then the shader is deleted.
			glDeleteShader(vs);
			glDeleteShader(fs);

			return true;
		}

		void Program::Use()
		{
			glUseProgram(m_id);
		}
	}
}
