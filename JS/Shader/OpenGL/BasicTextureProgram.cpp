#include "BasicTextureProgram.h"
#include "ShaderUtil.h"
#include "../../GraphicsAPI/OpenGL/OpenGLHeader.h"
#include "ShaderAttribute.h"
#include <iostream>

const char * JS::Shader::BasicTextureProgram::shader_code_vs = "\
attribute vec4    attPosition;\
attribute vec2    attTexcoord;\
attribute vec4    attColor;\
uniform mat4    uniMVPMatrix;\
varying vec2 uv;\
varying vec4 color;\
void main(void)\
{\
gl_Position = uniMVPMatrix * attPosition;\
uv = attTexcoord;\
color = attColor;\
}";

const char * JS::Shader::BasicTextureProgram::shader_code_fs = "\
varying vec2 uv;\
varying vec4 color;\
uniform sampler2D sTexture;\
void main (void)\
{\
gl_FragColor = texture2D(sTexture, uv) * color;\
}";

namespace JS
{
    namespace Shader
    {
        BasicTextureProgram::BasicTextureProgram()
        {
        }
        
        BasicTextureProgram::~BasicTextureProgram()
        {
        }
        
        void BasicTextureProgram::_BindAttributes()
        {
            std::map<uint32_t, std::string> mapAtt;
            
            mapAtt[ATT_POSITION] = "attPosition";
            mapAtt[ATT_TEXCOORD] = "attTexcoord";
            mapAtt[ATT_COLOR] = "attColor";
            
            JS::Shader::BindAttributeID(mapAtt, m_id);
        }
        
        void BasicTextureProgram::_BindUniforms()
        {
            std::map<uint32_t, std::string> mapUniform;
            
            mapUniform[UNI_MVPMatrix] = "uniMVPMatrix";
            mapUniform[UNI_TEXTURE] = "sTexture";
            
            JS::Shader::GetUniformID(m_mapBindUniform, mapUniform, m_id);
            
            for (auto & dd : m_mapBindUniform)
            {
                printf("BasicTextureProgram::_BindUniforms %u %d\n", dd.first, dd.second);
            }
        }
        
        void BasicTextureProgram::SetUniformMVPMatrix(const float * matrix)
        {
            glUniformMatrix4fv(m_mapBindUniform[UNI_MVPMatrix], 1, GL_FALSE, matrix);
        }
        
        void BasicTextureProgram::SetActiveTexture(const int & activetexture)
        {
            glUniform1i(m_mapBindUniform[UNI_TEXTURE], activetexture);
        }
    }
}
