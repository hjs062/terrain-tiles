#ifndef _JS_SHADER_ATTRIBUTE_H_HJS_20170323_
#define _JS_SHADER_ATTRIBUTE_H_HJS_20170323_

#include <cstdint>

namespace JS
{
    namespace Shader
    {
        enum ATTRIBUTE
        {
            ATT_POSITION = 0,
            ATT_TEXCOORD = 1,
            ATT_NORMAL = 2,
            ATT_COLOR = 3,
            ATT_4 = 4,
            ATT_5 = 5,
            ATT_6 = 6,
            ATT_7 = 7
        };
    }
}

#endif
