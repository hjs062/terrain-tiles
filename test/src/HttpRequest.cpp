#include <curl/curl.h>
#include "HttpRequest.h"

#include <cstdlib>
#include <cstring>

namespace terrain_tiles_test
{   
    HttpRequest::HttpRequest()
    {
        /* init the curl session */
        curl_handle = (CURL*)curl_easy_init();
        
        result.memory = nullptr;
        result.size = 0;
    }
    
    HttpRequest::~HttpRequest()
    {
        if(result.memory)
        {
            delete result.memory;
        }
        curl_easy_cleanup(curl_handle);
    }
    
    unsigned HttpRequest::WriteMemoryCallback(void *contents, unsigned size, unsigned nmemb, void *userp)
    {
        unsigned realsize = size * nmemb;
        Buffer *mem = (Buffer *)userp;
        
        mem->memory = (char*)realloc(mem->memory, mem->size + realsize + 1);
        if(mem->memory == NULL) {
            /* out of memory! */
            printf("not enough memory (realloc returned NULL)\n");
            return 0;
        }
        
        memcpy(&(mem->memory[mem->size]), contents, realsize);
        mem->size += realsize;
        
        return realsize;
    }
    
    void HttpRequest::operator()(const char * url)
    {
        if(result.memory)
        {
            delete result.memory;
            result.memory = nullptr;
            result.size = 0;
        }
        
        CURLcode res;
        
        /* specify URL to get */
        curl_easy_setopt(curl_handle, CURLOPT_URL, url);
        
        /* send all data to this function  */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        
        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&result);
        
        /* some servers don't like requests that are made without a user-agent
         field, so we provide one */
        curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
        
        /* get it! */
        res = curl_easy_perform(curl_handle);
        
        if(res != CURLE_OK)
        {
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
            
            if(result.memory)
            {
                delete result.memory;
                result.memory = nullptr;
                result.size = 0;
            }
        }
    }
}
