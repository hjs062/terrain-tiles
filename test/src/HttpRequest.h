#ifndef _HTTP_REQUEST_H_HJS_20180408_
#define _HTTP_REQUEST_H_HJS_20180408_

namespace terrain_tiles_test
{
    class CURL;
    class HttpRequest
    {
        class Buffer
        {
        public:
            char * memory;
            unsigned size;
            
        };
    public:
        HttpRequest();
        ~HttpRequest();
        void operator()(const char * url);
        
    private:
        static unsigned WriteMemoryCallback(void *contents, unsigned size, unsigned nmemb, void *userp);
        
    public:
        Buffer result;
        
    private:
        CURL *curl_handle;
        
    };
}

#endif
