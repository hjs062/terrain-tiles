#ifndef _JS_SHADER_MANAGER_H_HJS_20170323_
#define _JS_SHADER_MANAGER_H_HJS_20170323_

#include <cstdint>
#include <map>

#include "../../../JS/Shader/OpenGL/ShaderProgram.h"
#include "../../../JS/Shader/OpenGL/ShaderAttribute.h"

namespace terrain_tiles_test
{
    namespace Shader
    {
        enum class ecPROGRAM
        {
            BASIC_COLOR,
            BASIC_TEXTURE
        };
        
        class Manager
        {
        private:
            Manager();
            
        public:
            virtual ~Manager();
            static Manager * Instance();
            
            JS::Shader::Program* GetProgram(const ecPROGRAM & program);
            
        private:
            static Manager * m_Instance;
            
            std::map<ecPROGRAM, JS::Shader::Program*>  m_mapProgram;
        };
    }
}

#endif
